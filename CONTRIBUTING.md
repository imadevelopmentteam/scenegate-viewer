For information on contributing to the wiki and contributing open source code, visit these pages  
  
[Conduct Policy](https://bitbucket.org/imadevelopmentteam/scenegate-viewer/wiki/Policies%20and%20Procedures:/Our%20Conduct%20Policy)

[Privacy Policy](https://bitbucket.org/imadevelopmentteam/scenegate-viewer/wiki/Policies%20and%20Procedures:/Our%20Privacy%20Policy)

[Software Policies](https://bitbucket.org/imadevelopmentteam/scenegate-viewer/wiki/Policies%20and%20Procedures:/Our%20Software%20Policies)

[Coding Standards](https://bitbucket.org/imadevelopmentteam/scenegate-viewer/wiki/Development%20Resources:/Coding%20Standard)

[x-grid-info](https://bitbucket.org/imadevelopmentteam/scenegate-viewer/wiki/Development%20Resources:/x-grid-info)

[Build Instructions: Linux](https://bitbucket.org/imadevelopmentteam/scenegate-viewer/wiki/Build%20Instructions:/Linux) (in work)

[Build Instructions: Mac OS X](https://bitbucket.org/imadevelopmentteam/scenegate-viewer/wiki/Build%20Instructions:/Mac%20OS%20X) (coming soon) 

[Build Instructions: Windows 10](https://bitbucket.org/imadevelopmentteam/scenegate-viewer/wiki/Build%20Instructions:/Windows 10) 

[Contributing](https://bitbucket.org/imadevelopmentteam/scenegate-viewer/wiki/Development%20Resources:/Contributing)
  
  
Third Party License compliance information is available at
[Licensing](https://wiki.secondlife.com/wiki/Linden_Lab_Official:Second_Life_Viewer_Licensing_Program).

